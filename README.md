# addressGloria

Proyecto armado para el Proyecto Gloria.

# Overview

**Client-facing feature we ultimately want to support**

The ability for a user to give the app access to their address book so we can highlight which ones of their contacts are already on our service. The check should happen based on email and/or phone number.

**UX flow (simplified)**

- App asks user permission to get access to contacts
- App sends contacts data to server
- Server returns list of matching contacts who already use the app

**API functionality**

- user `one-to-many` contacts relationship
- add/remove/fetchByName users
- add/remove/fetchById contacts
- find contact data matches




