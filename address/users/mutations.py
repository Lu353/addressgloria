import graphene
from address.users.services import create_users 
from address.users.selectors import get_user_by_id

from address.contacts.selectors import get_contactData, get_contacts_by_id
from address.contacts.services import create_contact

class CreateUser(graphene.Mutation):
    name = graphene.String()
    userId = graphene.Int()

    class Arguments:
        name = graphene.String(required=True)

    @classmethod
    def mutate(cls, root, info, name):
        userId = create_users(name) 
        return CreateUser(userId=userId.userId, 
                          name=userId.name)


class DeleteUser(graphene.Mutation):
    ok = graphene.Boolean()
    class Arguments: 
        userId = graphene.ID(required=True)

    @classmethod
    def mutate(cls, root, info, userId):
        user = get_user_by_id(userId)
        ok = True if user else False
        if user:            
            user.delete()
        return DeleteUser(ok)