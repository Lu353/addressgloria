from django.db.models.fields import IntegerField
from graphene_django import DjangoObjectType
from address.models import Contacts, Users
import graphene  

class UsersType(DjangoObjectType):
    """
    Models to GraphQL: USERS
    """
    class Meta:
        model = Users
        fields = ("userId", 
                  "name", 
                  "contacts")


class contactData(graphene.ObjectType):
    type = graphene.String()
    value = graphene.String()


class ContactsType(DjangoObjectType):
    """
    Models to GraphQL: CONTACTS
    """
    userId = graphene.Int()
    contactData = graphene.List(contactData)

    def resolve_contactData(root, info):
        
        return [{"type": "email", "value": root.email},
                {"type": "phone", "value": root.phone}]

              
    def resolve_userId(root, info):
        return root.userId.userId if root.userId else None
    class Meta:
        model = Contacts
        fields = ("contactId", 
                  "name", 
                  "userId",
                  "contactData")
                  
