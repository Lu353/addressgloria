from django.db import models

# Create your models here.

class Users(models.Model):
    userId = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=150)
   
    class Meta:
        db_table = 'Users'

class Contacts(models.Model):
    contactId = models.BigAutoField(primary_key=True)
    userId = models.ForeignKey(
        Users, related_name="contacts", on_delete=models.SET_NULL,
        null=True
    )
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=280)
    phone = models.CharField(max_length=50)
    
    class Meta:
        db_table = 'Contacts'


    