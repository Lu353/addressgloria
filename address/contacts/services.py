from address.models import Contacts


def create_contact(userID: int, 
                   name: str, 
                   email: str, 
                   phone: str) -> Contacts:
    
    contact = Contacts.objects.create(userId_id=userID,
                                      name=name,
                                      email=email,
                                      phone=phone)
    return contact
