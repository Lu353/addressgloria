from address.contacts.selectors import get_contacts_by_id, get_contactData
from address.contacts.services import create_contact
from address.models import Contacts, Users
from django.test import TestCase


class TestCreateContact(TestCase):
    def setUp(self):
        user = Users.objects.create(userId=2, name="Luciana")

        _contact = Contacts.objects.create(contactId=2,
                                           name="Gloria",
                                           userId=user,
                                           email="gloria@gmail.com",
                                           phone="25865")

    def test_create_contact(self):

        contact = create_contact(userID=2,
                                 name="Ayelen",
                                 email="ayelen@gmail.com",
                                 phone="35126548")

        self.assertIsInstance(contact, Contacts)

class TestGetContact(TestCase):
    def setUp(self):
        user = Users.objects.create(userId=2, name="Luciana")

        _contact = Contacts.objects.create(contactId=2,
                                            name="Gloria",
                                            userId=user,
                                            email="gloria@gmail.com",
                                            phone="25865")

  
    def test_get_contacts_by_id(self):

        contact = get_contacts_by_id(contactId=2)
        self.assertIsInstance(contact, Contacts)
        self.assertEquals(contact.email,"gloria@gmail.com")
        self.assertEquals(contact.phone,"25865")

class TestGetContactData(TestCase):
    
    def setUp(self):
        user = Users.objects.create(userId=2, name="Luciana")

        _contact = Contacts.objects.create(contactId=2,
                                           name="Gloria",
                                           userId=user,
                                           email="gloria@gmail.com",
                                           phone="25865")


    

    def test_get_contactData(self):
        dict_test = {"contactData":[
                        {
                            "type":"phone",
                            "value":"123"
                        },
                        {
                            "type":"email",
                            "value":"test@test.com"
                        }
                    ]}

        email, phone= get_contactData(dict_test)
        
        self.assertEquals(email,"test@test.com")
        self.assertNotEquals(email,"test.com")
        self.assertEquals(phone,"123")
        self.assertNotEquals(phone,"123456")

