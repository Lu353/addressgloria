import graphene
from address.types import ContactsType
from address.contacts.selectors import get_contactData, get_contacts_by_id
from address.contacts.services import create_contact


class DeleteContacts(graphene.Mutation):
    ok = graphene.Boolean()

    class Arguments: 
        contactId = graphene.ID(required=True)

    @classmethod
    def mutate(cls, root, info, contactId):
        contact = get_contacts_by_id(contactId = contactId)
        ok = True if contact else False
        if contact:            
            contact.delete()
        return DeleteContacts(ok)


class InputContactData(graphene.InputObjectType):
    type = graphene.String()
    value = graphene.String()


class InputForMatch(graphene.InputObjectType):
    contactData = graphene.List(InputContactData)


class InputGeneral(graphene.InputObjectType):
    userId = graphene.ID()
    name = graphene.String()
    contactData = graphene.List(InputContactData)


class CreateContacts(graphene.Mutation):
    contacts = graphene.List(ContactsType)
    
    class Input:
        input = graphene.List(InputGeneral) 

    @classmethod
    def mutate(cls, root, info, input):
        contacts=[]
        for i in input:
            email, phone = get_contactData(i)

            contact = create_contact(i.get('userId'),
                                     i.get('name'), 
                                     email, 
                                     phone)

            contacts.append(contact)
        return CreateContacts(contacts) 


