from address.models import Contacts


def get_contactData(i: dict):

    for cd in i.get('contactData'):
        if cd.get('type') == "email":
            email = cd.get('value')    
            continue            
        elif cd.get('type') == "phone":
            phone = cd.get('value')
            continue
    return email, phone


def get_contacts_by_id(contactId: int) -> Contacts:
    contact = Contacts.objects.filter(contactId=contactId).last()
    return contact
