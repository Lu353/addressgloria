from django.core.checks import messages
import graphene
from graphene_django import DjangoObjectType

from address.types import UsersType, ContactsType
from address.contacts.mutations import CreateContacts, DeleteContacts, InputForMatch, get_contactData

from address.users.mutations import CreateUser, DeleteUser
from address.models import Contacts


class Query(graphene.ObjectType):
    getContact = graphene.Field(ContactsType, contactId=graphene.Int(required=True))
    matchContact = graphene.List(ContactsType, input=graphene.List(InputForMatch))

    def resolve_getContact(root, info, contactId):
        try:
            contact = Contacts.objects.get(contactId=contactId)
            return contact
        except Contacts.DoesNotExist:
            return None
    
    def resolve_matchContact(root, info, input):
        try: 
            contacts= []
            for i in input:

                email, phone = get_contactData(i)
                contact = Contacts.objects.filter(email=email,
                                                  phone=phone)
                contacts.append(contact)

            return contacts[0]

        except Contacts.DoesNotExist:
                    return None

    
class Mutation(graphene.ObjectType):
    # Users
    addUser = CreateUser.Field()
    removeUser = DeleteUser.Field()

    # Contacts
    addContacts = CreateContacts.Field()
    removeContact = DeleteContacts.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)

